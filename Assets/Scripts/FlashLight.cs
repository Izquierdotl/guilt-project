﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashLight : MonoBehaviour {

	public Light Flashlight;
	public AudioSource FlashLightSound;


	public AudioClip soundFlashOn;
	public AudioClip soundFlashOff;
	public bool Light = false;


	// Use this for initialization
	void Start () {
		Light = false;
		Flashlight.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown("f")) {
			if (Light == false) {
				Flashlight.enabled = true;
				Light = true;
				FlashLightSound.PlayOneShot (soundFlashOn);

			} 

			else if (Light == true) {
				Flashlight.enabled = false;
				Light = false;
				FlashLightSound.PlayOneShot (soundFlashOff);
			}

			
		}
	}
}


