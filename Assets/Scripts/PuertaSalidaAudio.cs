﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuertaSalidaAudio : MonoBehaviour {

	private AudioSource audioSource;
	public AudioClip[] audio;
	//public Text _bañoPrimeroTexto;


	//public AudioClip audioToalla

	// Use this for initialization
	void Start () {

		audioSource = GetComponent<AudioSource> ();
	}

	// Update is called once per frame
	void Update () {

	}

	public void NoSalir()
	{

		audioSource.PlayOneShot (audio [1]);
	}

	public void Puerta()
	{

		audioSource.PlayOneShot (audio [0]);
	}
}
