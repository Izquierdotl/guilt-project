﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour {

	public static bool doorKey;
	public bool open;
	public bool close;
	public bool inTrigger;
	public Animator Animator;

	void OnTriggerEnter (Collider other){
		inTrigger = true;
	}

	void OnTriggerExit (Collider other){
		inTrigger = false;
	}

	// Use this for initialization
	void Start () {
		
	}

	void Awake (){
		Animator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (inTrigger) {
			
			if (doorKey) {
				if (Input.GetKeyDown (KeyCode.E)) {
					/*open = true;
					close = false;*/
					Animator.SetBool ("Open", true);
				}

			}
		} else {
			if (Input.GetKeyDown (KeyCode.E)) {
				/*close = true;
				open = false;*/
				Animator.SetBool ("Open", false);
			}

		}
}


}