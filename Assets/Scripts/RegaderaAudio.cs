﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RegaderaAudio : MonoBehaviour {

	private AudioSource audioSource;
	public AudioClip[] audio;
	public Text _bañoPrimeroTexto;


	//public AudioClip audioToalla

	// Use this for initialization
	void Start () {

		audioSource = GetComponent<AudioSource> ();
	}

	// Update is called once per frame
	void Update () {
		
	}

	public void tomarUnBaño()
	{

		audioSource.PlayOneShot (audio [1]);
	}
	public void DondeEstaToalla()
	{

		audioSource.PlayOneShot (audio [0]);

		_bañoPrimeroTexto.gameObject.SetActive (true);
	}
}
